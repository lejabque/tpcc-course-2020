# Преподаватели

 Имя | Логин на gitlab
---- | ----
Роман Липовский | `Lipovsky`
Александр Гришутин | `rationalex`
Дмитрий Инютин | `inyutin`
Евгений Госткин | `gostkin`
Евгений Шлыков | `eshlykov`
Камиль Талипов | `kamiltalipov`
Мария Феофанова | `mary3000`
Никита Михайлов | `nikitamikhaylov`
Роман Санду | `Mrkol`
Степан Калинин | `MrKaStep`
Вадим Плахтинский | `VadimPl`
Михаил Анухин | `clumpytuna`
