#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/strand/spin_wait.hpp>

namespace solutions {

using twist::strand::SpinWait;

class TicketLock {
 public:
  // Don't change this method
  void Lock() {
    const size_t this_thread_ticket = next_free_ticket_.fetch_add(1);

    SpinWait spin_wait;
    while (this_thread_ticket != owner_ticket_.load()) {
      spin_wait();
    }
  }

  bool TryLock() {
    size_t current = next_free_ticket_.load();
    if (current == owner_ticket_.load()) {
      return next_free_ticket_.compare_exchange_strong(current, current + 1);
    } else {
      return false;
    }
  }

  // Don't change this method
  void Unlock() {
    owner_ticket_.store(owner_ticket_.load() + 1);
  }

 private:
  twist::stdlike::atomic<size_t> next_free_ticket_{0};
  twist::stdlike::atomic<size_t> owner_ticket_{0};
};

}  // namespace solutions
