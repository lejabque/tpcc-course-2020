#pragma once

#include <twist/stdlike/mutex.hpp>

namespace solutions {

// Automagically wraps all accesses to guarded object to critical sections
// Look at unit tests for API and usage examples

template <typename T>
class LockedPtr {
 public:
  LockedPtr(T* object, twist::stdlike::mutex* mutex)
      : object_(object), mutex_(mutex) {
  }

  T* operator->() {
    mutex_->lock();
    return object_;
  }

  ~LockedPtr() {
    mutex_->unlock();
  }

 private:
  T* object_;
  twist::stdlike::mutex* mutex_;
};

template <typename T>
class Guarded {
 public:
  LockedPtr<T> operator->() {
    return LockedPtr<T>(&object_, &mutex_);
  }

 private:
  T object_;
  twist::stdlike::mutex mutex_;
};

}  // namespace solutions
